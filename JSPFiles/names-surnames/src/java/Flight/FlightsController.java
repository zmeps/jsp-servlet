package Flight;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 
 */
import java.io.IOException;
import java.util.*; 

import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class FlightsController extends HttpServlet{
    
  public void doGet(HttpServletRequest request, HttpServletResponse response)   
                       throws IOException, ServletException {  
    Connection connection = null;  
    Statement stmt=null;  
    ResultSet rs=null;  
    String Choice = request.getParameter("mydropdown");
    
    List<Flight> dataList = new ArrayList<Flight>();  
    try {  
        // Load the JDBC driver  com.mysql.jdbc.Driver
        String driverName = "com.mysql.jdbc.Driver";  
        Class.forName(driverName);  
  
        // Create a connection to the database  
        String serverName = "localhost";  
        String portNumber = "3306";  
        //String sid = "xe";  
        String url = "jdbc:mysql://localhost:3306/airport"; 
        String username = "root";  
        String password = "";  
        connection = DriverManager.getConnection(url, username, password);  
  
        stmt = connection.createStatement();  
        //Statement st = connection.createStatement();
        String query;
        
        ////////////////////////////////////////////////
       
        if (request.getParameter("InsertData") != null) {
            
             insertData(request, response, connection);
        }
        else if(request.getParameter("DeleteData") != null)
        {
        
            String[] flights;
            String temp="";
            flights = request.getParameterValues("check");
            
            if (flights != null) 
            {
               for (int i=0; i < flights.length; i++) 
               {
                    temp=flights[i].replaceAll("\\s+","");
                    System.out.println("FLIGHTS : "+temp);
                    
                    String sql = "delete from flights where flightCode='"+temp+"'";
                    System.out.println("SQL QUERY "+sql);
                    PreparedStatement statement = connection.prepareStatement(sql);
                    int rowsDeleted = statement.executeUpdate();
                    //flights[i]+="'"+flights[i]+"'";
                    //PreparedStatement st = connection.prepareStatement("delete from flights where flightCode='CY600'");
                    //st.setString(1,flights[i]);
                    //st.executeUpdate();
                 
   
               }
               response.getWriter().println("Records Deleted successfully!");
            }
        }
        else if(request.getParameter("UpdateData") != null)
        {
            
            String [] UpdateFlight;
            UpdateFlight = request.getParameterValues("radio");
            
            System.out.println("UPDATE DATA... "+UpdateFlight[0]);
            request.setAttribute("data", UpdateFlight);  
            String strViewPage = "Updatehelp.jsp";  
            RequestDispatcher dispatcher = request.getRequestDispatcher(strViewPage);  
         
            request.setAttribute("UpdateFlight", UpdateFlight);
            if (dispatcher != null) {  
               dispatcher.forward(request, response);  
            } 
            
        }
        else if(request.getParameter("UpdateHelp") != null)
        {
           
            Flight updateFlight = new Flight();
            String insert= request.getParameter("insert"); 
            insert=insert.replaceAll("\\s+","");

            
            //temp1=.replaceAll("\\s+","");
            //temp2=request.getParameter("code").replaceAll("\\s+","");
            updateFlight.setCode(request.getParameter("code"));
            updateFlight.setDestination(request.getParameter("destination"));
                    
            String sql = "UPDATE flights SET flightCode='"+updateFlight.getCode()+"', flightDestination='"+updateFlight.getDestination()+"' WHERE flightCode='"+insert+"'";
            
            PreparedStatement statement = connection.prepareStatement(sql);
                    int rowsDeleted = statement.executeUpdate();
                    
            response.getWriter().println("Records Updated successfully!");
        }
        
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
         // Perform insert.
        if(Choice.equals("Insert")) {
            
             
             String site = new String("http://localhost:8080/names-surnames/InsertFlights.jsp");
             response.setStatus(response.SC_MOVED_TEMPORARILY);
             response.setHeader("Location", site); 
             
            
        }
        else if (Choice.equals("Delete")) 
        {    
            // Perform remove.
            rs = stmt.executeQuery("select * from flights");  
            while (rs.next()) {  
                
                //dataList.add(new Flight(rs.getInt("ID"), 
                     //   rs.getString("flightCode"), 
                     //   rs.getString("flightDestination")));
                Flight fl = new Flight(rs.getInt("ID"), rs.getString("flightCode"),rs.getString("flightDestination"));
                dataList.add(fl);
            }      
            
        }
        else if (Choice.equals("Update"))
        {
            rs = stmt.executeQuery("select * from flights");  
            while (rs.next()) {  
                
                //dataList.add(new Flight(rs.getInt("ID"), 
                  //      rs.getString("flightCode"), 
                    //    rs.getString("flightDestination")));   
                Flight fl = new Flight(rs.getInt("ID"), rs.getString("flightCode"),rs.getString("flightDestination"));
                dataList.add(fl);
            }      
        }
        else
        {
            // Perform show.   
            rs = stmt.executeQuery("select * from flights");  
            while (rs.next()) {  
               
                Flight fl = new Flight(rs.getInt("ID"), rs.getString("flightCode"),rs.getString("flightDestination"));
                dataList.add(fl);   
            }  
            
        }
        
        } catch (ClassNotFoundException e) {  
            // Could not find the database driver  
            e.printStackTrace();  
        } catch (SQLException e) {  
            // Could not connect to the database  
            e.printStackTrace();  
        } finally{  
            if(rs!=null){  
                try{   
                  rs.close();  
                }catch(Exception ex) { /* */   
                ex.printStackTrace();}  
            }  
            if(stmt!=null){  
                try{   
                  stmt.close();  
                }catch(Exception ex) { /* */  
                ex.printStackTrace();}  
            }  
            if(connection !=null){  
                try{   
                  connection.close();  
                }catch(Exception ex) { /* */   
                ex.printStackTrace();}  
            }  
         }  

    
        if (Choice.equals("Insert")) {
         // Perform insert.
         //query = "insert into flights (flightCode, flightDestination values"+
           //"("+ request.getParameter("flightcode") +","+ request.getParameter("flightdestination") +")";
         //rs = stmt.executeQuery(query);     
            
        }
        else if (Choice.equals("Delete")) 
        {    
            // Perform remove.
            //System.out.println("DIADIKASIA REMOVE...");
            // Perform show.   
            request.setAttribute("data", dataList);  
            String strViewPage = "DeleteFlights.jsp";  
            RequestDispatcher dispatcher = request.getRequestDispatcher(strViewPage);  
         
            if (dispatcher != null) {  
               dispatcher.forward(request, response);  
            }  
        }
        else if (Choice.equals("Update")) 
        {
           request.setAttribute("data", dataList);  
            String strViewPage = "UpdateFlights.jsp";  
            RequestDispatcher dispatcher = request.getRequestDispatcher(strViewPage);  
         
            if (dispatcher != null) {  
               dispatcher.forward(request, response);  
            }  
        
        }
        else
        {
            // Perform show.   
            request.setAttribute("data", dataList);  
            String strViewPage = "ShowFlights.jsp";  
            RequestDispatcher dispatcher = request.getRequestDispatcher(strViewPage);  
         
            if (dispatcher != null) {  
               dispatcher.forward(request, response);  
            }  
            
        }
        
   }  
  
  // Method to delete data
  
  private void insertData(HttpServletRequest request, HttpServletResponse response, Connection connection)
  {
            Flight insertFlight = new Flight();
            //insertFlight.setCode(url);
            
            //insertFlight.setId();
            insertFlight.setCode(request.getParameter("code"));
            insertFlight.setDestination(request.getParameter("destination"));
            
            String sql = "INSERT INTO flights (ID, flightCode, flightDestination) VALUES ( ?, ?, ?)";
 
            try
            {
                // call methods that might throw SQLException
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setInt(1, insertFlight.getId());
                statement.setString(2, insertFlight.getCode());
                statement.setString(3, insertFlight.getDestination());

                //statement.setString(4, "bill.gates@microsoft.com");

                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("A new user was inserted successfully!");
                    response.getWriter().println("A new record was inserted successfully!");
                }
            }
            catch (SQLException | IOException e)
            {
                // do something appropriate with the exception, *at least*:
                e.printStackTrace();
            }
            
  }
  
}  
