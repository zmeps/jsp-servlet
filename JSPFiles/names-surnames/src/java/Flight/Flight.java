package Flight;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 
 */
public class Flight {
    
    private int id;
    private String code;
    private String destination;
    
    public Flight()
    {
       this.id = 999;
       this.code = "CY999";
       this.destination = "N/A";
    }
    
    public Flight(int id, String code, String destination)
    {
       this.id = id;
       this.code = code;
       this.destination = destination;
    }
    
    public void setId(int id)
    {
       if(id>0)
           this.id=id;
       else
           this.id=999;
    }
    
    public void setCode(String code)
    {  
       int len;
       len=code.length(); 
       String upToNCharacters = code.substring(0, Math.min(code.length(), 2));
       
       if(len==4||len==5)
       {
          if(code.equals("CY"))
          {
             this.code=code;
          }  
       }
       else
       {
          this.code="CY999";
       }
           
    }
     
    public void setDestination(String destination)
    {
       this.destination = destination;
    }
      
    public int getId()
    {
       return this.id;
    }
       
    public String getCode()
    {
       return this.code;
    }
        
    public String getDestination()
    {
       return this.destination;
    }
     

    public String toString(){
      return  "Value Id : "+this.id+"Value Code : "+this.code+"Value destination : "+this.destination;  
    }
  
}
